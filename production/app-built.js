var MadfolketApp = angular.module('Madfolket', [
	'ionic',
	'ngCordova',
	'facebook',

	'AuthenticationModule',
	'RecipesModule'
]);

MadfolketApp.config(function(FacebookProvider) {
	FacebookProvider.init('1488234771462602');
});

MadfolketApp.run(function ($ionicPlatform, $location, $rootScope, $cordovaStatusbar, $ionicLoading, $cordovaSplashscreen, $interval, AuthenticationService) {

	$rootScope.api = 'http://' + $location.host() + '/madfolket-server/public';
	//$rootScope.api = 'http://test.madfolket.dk/madfolket-server/public';

	//AuthenticationService.updateLoggedIn();

	$rootScope.getSlugified = function(string) {
		return $filter('slugify')(string);
	};

	$rootScope.isLoggedIn = function() {
		return AuthenticationService.isLoggedIn();
	};

	$rootScope.getUser = function() {
		return AuthenticationService.getUser();
	};

	$rootScope.$on('$stateChangeStart', function() {
		$ionicLoading.show({
			template: '<i class="icon ion-load-c"></i>',
			animation: 'fade-in',
			showBackdrop: true,
			showDelay: 300
		});
	});

	$rootScope.$on('$stateChangeSuccess', function() {
		$ionicLoading.hide()
	});

	$ionicPlatform.ready(function () {

		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}

		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
			$cordovaStatusbar.overlaysWebView(true);
			$cordovaStatusBar.style(1); //Light
			$cordovaStatusbar.styleColor('white');

		}
	});
});






MadfolketApp.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

	// With the new view caching in Ionic, Controllers are only called
	// when they are recreated or on app start, instead of every page change.
	// To listen for when this page is active (for example, to refresh data),
	// listen for the $ionicView.enter event:
	//$scope.$on('$ionicView.enter', function(e) {
	//});

	// Form data for the login modal
	$scope.loginData = {};

	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('templates/login.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	// Triggered in the login modal to close it
	$scope.closeLogin = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.login = function() {
		$scope.modal.show();
	};

	// Perform the login action when the user submits the login form
	$scope.doLogin = function() {
		console.log('Doing login', $scope.loginData);

		// Simulate a login delay. Remove this and replace with your login
		// code if using a login system
		$timeout(function() {
			$scope.closeLogin();
		}, 1000);
	};
})

MadfolketApp.controller('PlaylistsCtrl', function($scope) {
		$scope.playlists = [
			{ title: 'Reggae', id: 1 },
			{ title: 'Chill', id: 2 },
			{ title: 'Dubstep', id: 3 },
			{ title: 'Indie', id: 4 },
			{ title: 'Rap', id: 5 },
			{ title: 'Cowbell', id: 6 }
		];
	})

MadfolketApp.controller('PlaylistCtrl', function($scope, $stateParams) {
	});
MadfolketApp.config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app', {
				url: '/app',
				abstract: true,
				templateUrl: 'templates/menu.html'
			})

			.state('app.recipes', {
				url: '/recipes',
				views: {
					'menuContent': {
						templateUrl: 'js/modules/recipes/partials/recipes.html',
						controller: 'RecipesController',
						resolve: {
							recipes : ['RecipesService', '$q', function(RecipesService, $q) {
								var defer = $q.defer();

								RecipesService.getRecipes().then(function(response) {
									console.log(response.data);
									defer.resolve(response.data);
								});

								return defer.promise;
							}]
						}
					}
				}
			})

			.state('app.recipe', {
				url: '/recipes/:recipeId',
				views: {
					'menuContent': {
						templateUrl: 'js/modules/recipes/partials/recipe.html',
						controller: 'RecipeController',
						resolve: {
							data : ['RecipesService', '$q', '$stateParams', function(RecipesService, $q, $stateParams) {
								var defer = $q.defer();

								RecipesService.getRecipe($stateParams.recipeId).then(function(response) {
									console.log(response.data);
									defer.resolve(response.data);
								});

								return defer.promise;
							}]
						}
					}
				}
			})
			.state('app.search', {
				url: '/search',
				views: {
					'menuContent': {
						templateUrl: 'templates/search.html'
					}
				}
			})

			.state('app.browse', {
				url: '/browse',
				views: {
					'menuContent': {
						templateUrl: 'templates/browse.html'
					}
				}
			})
			.state('app.playlists', {
				url: '/playlists',
				views: {
					'menuContent': {
						templateUrl: 'templates/playlists.html',
						controller: 'PlaylistsCtrl'
					}
				}
			})

			.state('app.single', {
				url: '/playlists/:playlistId',
				views: {
					'menuContent': {
						templateUrl: 'templates/playlist.html',
						controller: 'PlaylistCtrl'
					}
				}
			});
		// if none of the above states are matched, use this as the fallback
		$urlRouterProvider.otherwise('/app/recipes');

		//.state('app', {
		//	url: '/app',
		//	abstract: true,
		//	templateUrl: 'templates/menu.html',
		//	controller: 'AppCtrl'
		//})
		//
		//.state('app.recipes', {
		//	url: '/recipes',
		//	views: {
		//		'menuContent': {
		//			templateUrl: 'js/modules/recipes/partials/recipes.html',
		//			controller: 'RecipesController',
		//			resolve: {
		//				recipes : ['RecipesService', '$q', function(RecipesService, $q) {
		//					var defer = $q.defer();
		//
		//					RecipesService.getRecipes().then(function(response) {
		//						console.log(response.data);
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			}
		//		}
		//	}
		//});
		//
		//$urlRouterProvider.otherwise('/recipes');

		//.state('tab.recipe', {
		//	url: '/recipes/:recipeId',
		//	views: {
		//		'tab-recipes': {
		//			templateUrl: 'js/modules/recipes/partials/recipe.html',
		//			controller: 'RecipeController',
		//			resolve: {
		//				data : ['RecipesService', '$q', '$stateParams', function(RecipesService, $q, $stateParams) {
		//					var defer = $q.defer();
		//
		//					RecipesService.getRecipe($stateParams.recipeId).then(function(response) {
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			},
		//
		//		}
		//	}
		//})
		//
		//.state('tab.shopping-lists', {
		//	url: '/shopping-lists',
		//	views: {
		//		'tab-shoppinglists': {
		//			templateUrl: "js/modules/shopping-list/partials/shopping-lists.html",
		//			controller: 'ShoppingListsController',
		//			resolve: {
		//
		//				shoppingLists : ['ShoppingListService', '$q', function(ShoppingListService, $q) {
		//					var defer = $q.defer();
		//
		//					console.log('getting');
		//
		//					ShoppingListService.getShoppingLists().then(function(response) {
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			},
		//
		//		}
		//	}
		//})
		//
		//.state('tab.shopping-list', {
		//	url: '/shopping-lists/:shoppingListId',
		//	views: {
		//		'tab-shoppinglists': {
		//			templateUrl: "js/modules/shopping-list/partials/shopping-list.html",
		//			controller: 'ShoppingListController',
		//			resolve: {
		//				shoppingList: ['ShoppingListService', '$q', '$stateParams', function(ShoppingListService, $q, $stateParams) {
		//					var defer = $q.defer();
		//
		//					ShoppingListService.getShoppingList($stateParams.shoppingListId).then(function(response) {
		//					console.log('aa');
		//
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			},
		//		}
		//	}
		//})
		//
		//.state('user', {
		//	url: '/user',
		//	template: 'user',
		//});

	}
]);
MadfolketApp.directive('ratings', ['RecipesService', function(RecipesService){

	return {

		scope: {
		 	stars: '=',
		 	changable: '=?',
		 	myrating: '=',
		 	recipe: '=',
		},

		restrict: 'E',
		templateUrl: 'js/common/directives/templates/ratings.html',

		link: function($scope) {

			$scope.starsArray = [];
			$scope.currentStars = $scope.stars;
			$scope.haveRated = angular.isNumber($scope.myrating);
			$scope.hovering = false;

			for (i = 0; i < 5; i++) {
				$scope.starsArray.push({filled: false});
			}

			$scope.hoverIn = function(hoveredStar) {
			    if (!$scope.$root.isLoggedIn())
			        return;

				$scope.hovering = true;
				setStars($scope.starsArray.indexOf(hoveredStar) + 1);
			};

			$scope.hoverOut = function() {
				$scope.hovering = false;

				// Set stars to the amount before hovering
				setStars($scope.currentStars);
			};

			$scope.rate = function(hoveredStar) {
			    if (!$scope.$root.isLoggedIn())
			        return;

				var star = $scope.starsArray.indexOf(hoveredStar) + 1;

				// Check if you are trying to reset your rating
				if ($scope.myrating == star) {
					$scope.setStars = $scope.stars;
					$scope.currentStars = $scope.stars;
					$scope.myrating = null;
					$scope.haveRated = false;

					RecipesService.rateRecipe($scope.recipe, $scope.myrating).then(function(response) {
						$scope.currentStars = response.data;
						setStars(response.data);
					});
				}

				// If not, set your rating
				else {
					$scope.myrating = star;
					$scope.currentStars = star;
					$scope.haveRated = true;

					RecipesService.rateRecipe($scope.recipe, $scope.myrating);
				}
			};

			// Set amount of filled stars
			var setStars = function(stars) {

				angular.forEach($scope.starsArray, function(star) {
					if ($scope.starsArray.indexOf(star) <= stars - 1) {
						star.filled = true;
					}
					else {
						star.filled = false;
					}
				});

			};

			// Initialize original amount of stars
			setStars($scope.stars);

			if ($scope.myrating > 0) {
				$scope.haveRated = true;
				$scope.currentStars = $scope.myrating;
				setStars($scope.myrating);
			}


		}
	};
}]);
var AuthenticationModule = angular.module('AuthenticationModule', []);
var CookbooksModule = angular.module('CookbooksModule', []);
var RecipesModule = angular.module('RecipesModule', [])

.run(['$rootScope', function($rootScope) {

	$rootScope.convertSecondsToTime = function(seconds) {
	    return {
			hours: Math.floor(seconds / 3600),
			minutes: parseInt(seconds / 60) % 60,
			seconds: parseInt(seconds % 60, 10)
	    }
	}

}]);
var UsersModule = angular.module('UsersModule', []);
var NotificationsModule = angular.module('NotificationsModule', []);
var ShoppingListModule = angular.module('ShoppingListModule', []);
var SinglepagesModule = angular.module('SinglepagesModule', []);
AuthenticationModule.controller('AuthenticationController', function($scope, AuthenticationService, AuthenticationFacebookService) {

    $scope.login = function() {

    	AuthenticationFacebookService.login().then(function(response) {   
            if (response.authResponse) {

	            var token = response.authResponse.accessToken;
		        return AuthenticationService.login(token);

            }
    	});

    };

    $scope.logout = function() {
        return AuthenticationService.logout();
    }

    $scope.isLoggedIn = function() {
        return AuthenticationService.isLoggedIn();
    }
});

AuthenticationModule.service('AuthenticationFacebookService', ['$rootScope', 'Facebook',
	function($rootScope, Facebook) {

		this.login = function() {

	        return Facebook.login(function(response) {
	        }, {scope: 'email'});
		};

		this.logout = function() {
			Facebook.logout(function(response) {
				console.debug('OK');
			}, function(response) {
				console.debug('Noes');
			});
		}



	}
]);
AuthenticationModule.service('AuthenticationService', ['$rootScope', '$http', 'AuthenticationFacebookService',
	function($rootScope, $http, AuthenticationFacebookService) {

		var self = this;

		this.loggedIn = false;
		this.user = null;

		this.login = function(token) {

			return $http.post($rootScope.api + '/authentication/login', {token: token}).then(function(response) {
				console.log(response);
				self.loggedIn = true;
				self.user = response.data;
				console.debug(self.loggedIn);
			});

		};

		this.logout = function() {
			$http.post($rootScope.api + '/authentication/logout').then(function(response) {
				AuthenticationFacebookService.logout();
				self.loggedIn = false;
			});
		};


	    this.updateLoggedIn = function() {



			$http.get($rootScope.api + '/authentication/check-login').then(function(response) {
				console.log('You are logged in');
				self.loggedIn = true;
				self.user = response.data;
			}, function(response) {
				console.log('You are not logged in');
				self.loggedIn = false;
			});

	    };

	    this.isLoggedIn = function() {
	    	return self.loggedIn;
	    }

	    this.getUser = function() {
	    	return self.user;
	    }


	}
]);
CookbooksModule.controller('CookbookController', function($scope) {

    $scope.cookbookName = "Patricks Konfirmation"

    $scope.recipes = [
        { id: 1, description: 'En lækker pizza fra det sydlige Italien', active: false, author: 'Patrick Reck', title: 'Pizza Maguarita', time: '25 min', stars: 3, picture: 'http://pinnest.net/newpinnest/wp-content/uploads/2013/08/137724465553a2c.jpg' },
        { id: 2, description: 'Sushi som var det var en Sushi-mester!', active: false, author: 'Nicklas Kevin Frank', title: 'Advocado sushi rolls', time: '2 t 30 min', stars: 5, picture: 'http://www.madogbolig.dk/~/media/websites/madogbolig.dk/Website/Gourmet/gourmetmadpaabudget_p.ashx' },
        { id: 3, description: 'Krabber fra Jensens Fiskerestaurant', active: false, author: 'Lisbeth Olesen', title: 'Krabbe med radisser', time: '50 min', stars: 4, picture: 'http://multimedia.pol.dk/archive/00602/iStock_kammusling_r_602603a.jpg' },
        { id: 4, description: 'Grillet mørbrad med hvidløg. Perfekt til sommeren', active: false, author: 'Anna Marie', title: 'Grillet mørbrad', time: '1 t 15 min', stars: 1, picture: 'http://images.fyens.dk/67/914467_608_608_0_109_2126_1195.jpg' },

        { id: 5, description: 'Pandekagerne som farmor lavede dem', active: false, author: 'Bob the Panmaker', title: 'Pandekager', time: '3 t', stars: 2, picture: 'http://www.pamelasproducts.com/wp-content/uploads/2012/09/Pancakes.jpg' },
        { id: 6, description: 'En Kaj kage som alle kan være med på', active: false, author: 'Den skaldede kok', title: 'Kaj kage', time: '30 min', stars: 1, picture: 'http://2.bp.blogspot.com/-3Cq9NXQ7lz8/Tc_bmG4eaQI/AAAAAAAAACk/1xRQ-q0EthE/s320/HPIM1644.JPG' },
        { id: 7, description: 'Jødekager som kun Thomas kan lave dem', active: false, author: 'Thomazi Bornerup', title: 'Mors jødekager', time: '19 t 45 min', stars: 5, picture: 'http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/19410/4_300.jpg' },
        { id: 8, description: 'Citron fromages over alle citron fromager', active: false, author: 'Mikes mor', title: 'Citron fromage', time: '5 t 10 min', stars: 4, picture: 'http://www.madkogebogen.dk/billeder/citronfromage1_large.jpg' },
    ];

    $scope.toggleActive = function(recipe) {
        recipe.active = !recipe.active;
    }

        $scope.user = { id: 2, name: "Nicklas Kevin Frank", badges: 6, recipe_count: 9, image: "https://scontent-a-ams.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/10523213_10204714401085620_2066924486547918154_n.jpg?oh=0fefc67a51898e9130e0e61f37da1232&oe=54C04F50"};

        $scope.cookbooks = [
        { description: 'En lækker pizza fra det sydlige Italien', active: false, author: 'Patrick Reck', title: 'Mad til gæster', time: '25 min', stars: 3, picture: 'http://pinnest.net/newpinnest/wp-content/uploads/2013/08/137724465553a2c.jpg' },
        { description: 'Sushi som var det var en Sushi-mester!', active: false, author: 'Nicklas Kevin Frank', title: 'Børnefødselsdage', time: '2 t 30 min', stars: 5, picture: 'http://www.madogbolig.dk/~/media/websites/madogbolig.dk/Website/Gourmet/gourmetmadpaabudget_p.ashx' },
        { description: 'Krabber fra Jensens Fiskerestaurant', active: false, author: 'Lisbeth Olesen', title: 'Italiens goder', time: '50 min', stars: 4, picture: 'http://multimedia.pol.dk/archive/00602/iStock_kammusling_r_602603a.jpg' },
        { description: 'Grillet mørbrad med hvidløg. Perfekt til sommeren', active: false, author: 'Anna Marie', title: 'Ost ost ost!', time: '1 t 15 min', stars: 1, picture: 'http://images.fyens.dk/67/914467_608_608_0_109_2126_1195.jpg' },

        { description: 'Pandekagerne som farmor lavede dem', active: false, author: 'Bob the Panmaker', title: 'Patrick har fødselsdag', time: '3 t', stars: 2, picture: 'http://www.pamelasproducts.com/wp-content/uploads/2012/09/Pancakes.jpg' },
        { description: 'En Kaj kage som alle kan være med på', active: false, author: 'Den skaldede kok', title: 'Mad ungerne kan lide', time: '30 min', stars: 1, picture: 'http://2.bp.blogspot.com/-3Cq9NXQ7lz8/Tc_bmG4eaQI/AAAAAAAAACk/1xRQ-q0EthE/s320/HPIM1644.JPG' },
        { description: 'Jødekager som kun Thomas kan lave dem', active: false, author: 'Thomazi Bornerup', title: 'Desertamums', time: '19 t 45 min', stars: 5, picture: 'http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/19410/4_300.jpg' },
        { description: 'Citron fromages over alle citron fromager', active: false, author: 'Mikes mor', title: 'Dømt i gryderetten', time: '5 t 10 min', stars: 4, picture: 'http://www.madkogebogen.dk/billeder/citronfromage1_large.jpg' },
    ];
    
    // Kun til demo af siden uden backend

    $scope.lastPictures = [];
    $scope.randomPicture = function() {

        var foundPicture = false;
        var picture = null;
        
        while (!foundPicture) {

            var tempPicture = $scope.getRandomPicture();

            var duplicate = false;

            angular.forEach($scope.lastPictures, function(lastPicture) {
               if (lastPicture == tempPicture) {
                   duplicate = true;
               } 
            });            
            
            if (!duplicate) {
                foundPicture = true;
                picture = tempPicture;
            }
        }
        
        if ($scope.lastPictures.length == 4) {
            $scope.lastPictures = [];            
        }
        
        $scope.lastPictures.push(picture);

        return picture;
    }
    
    $scope.getRandomPicture = function() {
        var picture = $scope.cookbooks[Math.floor(Math.random() * $scope.cookbooks.length)].picture;
        return picture;
    }
    
    
        angular.forEach($scope.cookbooks, function(cookbook) {
        cookbook.pictures = [];
        
        for (i = 0; i < 4; i++) { 
        
            cookbook.pictures.push($scope.randomPicture());
            
        }
         
    });
    
})
CookbooksModule.controller('CookbooksController', function($scope) {

	$scope.cookbooks = [
		{ id: 1, description: 'En lækker pizza fra det sydlige Italien', active: false, author: 'Patrick Reck', title: 'Mad til gæster', time: '25 min', stars: 3, picture: 'http://pinnest.net/newpinnest/wp-content/uploads/2013/08/137724465553a2c.jpg' },
		{ id: 2, description: 'Sushi som var det var en Sushi-mester!', active: false, author: 'Nicklas Kevin Frank', title: 'Børnefødselsdage', time: '2 t 30 min', stars: 5, picture: 'http://www.madogbolig.dk/~/media/websites/madogbolig.dk/Website/Gourmet/gourmetmadpaabudget_p.ashx' },
		{ id: 3, description: 'Krabber fra Jensens Fiskerestaurant', active: false, author: 'Lisbeth Olesen', title: 'Italiens goder', time: '50 min', stars: 4, picture: 'http://multimedia.pol.dk/archive/00602/iStock_kammusling_r_602603a.jpg' },
		{ id: 4, description: 'Grillet mørbrad med hvidløg. Perfekt til sommeren', active: false, author: 'Anna Marie', title: 'Ost ost ost!', time: '1 t 15 min', stars: 1, picture: 'http://images.fyens.dk/67/914467_608_608_0_109_2126_1195.jpg' },

		{ id: 5, description: 'Pandekagerne som farmor lavede dem', active: false, author: 'Bob the Panmaker', title: 'Patrick har fødselsdag', time: '3 t', stars: 2, picture: 'http://www.pamelasproducts.com/wp-content/uploads/2012/09/Pancakes.jpg' },
		{ id: 6, description: 'En Kaj kage som alle kan være med på', active: false, author: 'Den skaldede kok', title: 'Mad ungerne kan lide', time: '30 min', stars: 1, picture: 'http://2.bp.blogspot.com/-3Cq9NXQ7lz8/Tc_bmG4eaQI/AAAAAAAAACk/1xRQ-q0EthE/s320/HPIM1644.JPG' },
		{ id: 7, description: 'Jødekager som kun Thomas kan lave dem', active: false, author: 'Thomazi Bornerup', title: 'Desertamums', time: '19 t 45 min', stars: 5, picture: 'http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/19410/4_300.jpg' },
		{ id: 8, description: 'Citron fromages over alle citron fromager', active: false, author: 'Mikes mor', title: 'Dømt i gryderetten', time: '5 t 10 min', stars: 4, picture: 'http://www.madkogebogen.dk/billeder/citronfromage1_large.jpg' },
	];
    
    // Kun til demo af siden uden backend

    $scope.lastPictures = [];
    $scope.randomPicture = function() {

        var foundPicture = false;
        var picture = null;
        
        while (!foundPicture) {

            var tempPicture = $scope.getRandomPicture();

            var duplicate = false;

            angular.forEach($scope.lastPictures, function(lastPicture) {
               if (lastPicture == tempPicture) {
                   duplicate = true;
               } 
            });            
            
            if (!duplicate) {
                foundPicture = true;
                picture = tempPicture;
            }
        }
        
        if ($scope.lastPictures.length == 4) {
            $scope.lastPictures = [];            
        }
        
        $scope.lastPictures.push(picture);

        return picture;
    }
    
    $scope.getRandomPicture = function() {
        var picture = $scope.cookbooks[Math.floor(Math.random() * $scope.cookbooks.length)].picture;
        return picture;
    }
    
    
        angular.forEach($scope.cookbooks, function(cookbook) {
        cookbook.pictures = [];
        
        for (i = 0; i < 4; i++) { 
        
            cookbook.pictures.push($scope.randomPicture());
            
        }
         
    });
})
NotificationsModule.controller('NotificationsController', function($scope, NotificationsService) {

    $scope.notifications = ""

    $scope.getNotifications = function(userId){
        NotificationsService.getNotifications(userId).then(function(response) {
            $scope.notifications = response.data;
        });
    }

});

NotificationsModule.service('NotificationsService', ['$rootScope', '$http', 
	function($rootScope, $http) {
		
		this.getNotifications = function(userId) {
			return $http.get($rootScope.api + '/notifications/notifications/' + userId);
		}
	}
]);
RecipesModule.controller('CategoryController', function($scope) {

	$scope.categories = [

		{ 
			id: 1, name: 'Aftensmad', 
			recipes: [
				{ id: 1, description: 'En lækker pizza fra det sydlige Italien', active: false, author: 'Patrick Reck', title: 'Pizza Maguarita', time: '25 min', stars: 3, picture: 'http://pinnest.net/newpinnest/wp-content/uploads/2013/08/137724465553a2c.jpg' },
				{ id: 2, description: 'Sushi som var det var en Sushi-mester!', active: false, author: 'Nicklas Kevin Frank', title: 'Advocado sushi rolls', time: '2 t 30 min', stars: 5, picture: 'http://www.madogbolig.dk/~/media/websites/madogbolig.dk/Website/Gourmet/gourmetmadpaabudget_p.ashx' },
				{ id: 3, description: 'Krabber fra Jensens Fiskerestaurant', active: false, author: 'Lisbeth Olesen', title: 'Krabbe med radisser', time: '50 min', stars: 4, picture: 'http://multimedia.pol.dk/archive/00602/iStock_kammusling_r_602603a.jpg' },
				{ id: 4, description: 'Grillet mørbrad med hvidløg. Perfekt til sommeren', active: false, author: 'Anna Marie', title: 'Grillet mørbrad', time: '1 t 15 min', stars: 1, picture: 'http://images.fyens.dk/67/914467_608_608_0_109_2126_1195.jpg' },
			]
		},

		{ 
			id: 2, name: 'Desserter', 
			recipes: [
				{ id: 5, description: 'Pandekagerne som farmor lavede dem', active: false, author: 'Bob the Panmaker', title: 'Pandekager', time: '3 t', stars: 2, picture: 'http://www.pamelasproducts.com/wp-content/uploads/2012/09/Pancakes.jpg' },
				{ id: 6, description: 'En Kaj kage som alle kan være med på', active: false, author: 'Den skaldede kok', title: 'Kaj kage', time: '30 min', stars: 1, picture: 'http://2.bp.blogspot.com/-3Cq9NXQ7lz8/Tc_bmG4eaQI/AAAAAAAAACk/1xRQ-q0EthE/s320/HPIM1644.JPG' },
				{ id: 7, description: 'Jødekager som kun Thomas kan lave dem', active: false, author: 'Thomazi Bornerup', title: 'Mors jødekager', time: '19 t 45 min', stars: 5, picture: 'http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/19410/4_300.jpg' },
				{ id: 8, description: 'Citron fromages over alle citron fromager', active: false, author: 'Mikes mor', title: 'Citron fromage', time: '5 t 10 min', stars: 4, picture: 'http://www.madkogebogen.dk/billeder/citronfromage1_large.jpg' },
			]
		},

		{ 
			id: 3, name: 'Morgenmad', 
			recipes: [
				{ id: 2, description: 'Sushi som var det var en Sushi-mester!', active: false, author: 'Nicklas Kevin Frank', title: 'Advocado sushi rolls', time: '2 t 30 min', stars: 5, picture: 'http://www.madogbolig.dk/~/media/websites/madogbolig.dk/Website/Gourmet/gourmetmadpaabudget_p.ashx' },
				{ id: 1, description: 'En lækker pizza fra det sydlige Italien', active: false, author: 'Patrick Reck', title: 'Pizza Maguarita', time: '25 min', stars: 3, picture: 'http://pinnest.net/newpinnest/wp-content/uploads/2013/08/137724465553a2c.jpg' },
				{ id: 5, description: 'Pandekagerne som farmor lavede dem', active: false, author: 'Bob the Panmaker', title: 'Pandekager', time: '3 t', stars: 2, picture: 'http://www.pamelasproducts.com/wp-content/uploads/2012/09/Pancakes.jpg' },
				{ id: 6, description: 'En Kaj kage som alle kan være med på', active: false, author: 'Den skaldede kok', title: 'Kaj kage', time: '30 min', stars: 1, picture: 'http://2.bp.blogspot.com/-3Cq9NXQ7lz8/Tc_bmG4eaQI/AAAAAAAAACk/1xRQ-q0EthE/s320/HPIM1644.JPG' },
			]
		}


	];

	$scope.toggleActive = function(recipe) {
		recipe.active = !recipe.active;
	}

})
RecipesModule.controller('CreateRecipeController', function($scope, $sce, RecipesService, $modal, orderByFilter, $upload, $state) {

	/* Recipe object to be submitted to the server, simulates actual recipe object */
	$scope.recipe = {
		freezable: false,
		persons: 4,
		steps: [
		],
		ingredients: [
		],
		tags: [
		],
		images: [		
		],
	};

	/* Steps */
	$scope.steps = [
		{ name: 'Basale informationer', icon: 'ion-clipboard', tips: 'For at få flest mulige besøgende på din opskrift, bør du lave en fangende introduktion. <br/><br/>Derudover anbefaler vi at vælge et præcist navn.' },
		{ name: 'Kategorisering', icon: 'ion-earth', tips: 'For at kunne kategorisere din opskrift bedst muligt, skal du angive oprindelseslandet og en række kategorier.' },
		{ name: 'Ingredienser', icon: 'ion-pizza', tips: 'Her skal du indtaste de ingredienser der er brug for i din opskrift.<br/><br/>Du kan lave <b>opdelinger</b> til f.eks. Kød og Sovs eller Kage og Glasur.' },
		{ name: 'Vejledning', icon: 'ion-ios7-browsers', tips: 'For at guide dine læsere bedst igennem din opskrift, skal du indtaste den i <b>skridt</b>.<br/><br/>Ud for hvert skridt kan du aktivere et <b>æggeur</b> der gør det lættere for din læser at tage tid. Det kan f.eks. være hvis din kage skal bages i 20 minutter.' },
		{ name: 'Billeder', icon: 'ion-image', tips: 'Din opskrift vil få flere besøgende hvis der er billeder af resultatet. Det er dog <b>valgfrit</b> at ligge billeder op. <br/><br/> Når du har lagt mere end 1 billede op, kan du vælge hovedbilledet (det billede der vil blive vist først) ved at <b>klikke på miniaturen</b>.' }
	];
	$scope.currentStep = 0;

	$scope.nextStep = function() {
		if ($scope.currentStep !== $scope.steps.length - 1)
			$scope.currentStep++;
	}
	$scope.previousStep = function() {
		if ($scope.currentStep !== 0)
			$scope.currentStep--;
	}
    $scope.setStep = function(step) {
		$scope.currentStep = step;
	}

	/* ------ STEP 00 ------ */
	$scope.persons = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

	/* ------ STEP 01 ------ */

	/* Get origins and handle */
	$scope.origins = null;
	RecipesService.getOrigins().then(function(response) {
		$scope.origins = response.data;
		$scope.recipe.origin = $scope.origins[42];
	});


	/* Get categories and handle */
	$scope.categories = null;
	RecipesService.getCategories().then(function(response) {
		$scope.categories = response.data;
		$scope.recipe.category = $scope.categories[0];
	})

	/* Get tags and handle */
	$scope.tags = [];

	var reorderTags = function() {	$scope.tags = orderByFilter($scope.tags, 'name'); }

	RecipesService.getTags().then(function(response) {

		angular.forEach(response.data, function(category) {

			angular.forEach(category.tags, function(tag) {
				tag.type = category.name;
				$scope.tags.push(tag);
			})
		});

		reorderTags();

		$scope.currentTag = $scope.tags[0];
	})

	$scope.addTag = function() {
		$scope.recipe.tags.push($scope.currentTag);

		var index = $scope.tags.indexOf($scope.currentTag);

		$scope.tags.splice(index, 1);

		reorderTags();

		$scope.currentTag = $scope.tags[0];
	}

	$scope.removeTag = function(tag) {
		$scope.tags.push(tag);
	
		var index = $scope.recipe.tags.indexOf(tag);
		$scope.recipe.tags.splice(index, 1);

		reorderTags();
	}
	/* ------/ STEP 01 /------ */



	/* ------ STEP 02 ------ */

	/* Handle ingredients */
	$scope.currentIngredient = {};

	$scope.chosenIngredient = undefined; 
	$scope.getIngredients = function(val) {
		return RecipesService.getIngredients(val).then(function(response) {
			return response.data;
		});
	}
	$scope.$watch('chosenIngredient', function(ingredient) {
		$scope.currentIngredient.name = ingredient; 
	});


	$scope.units = undefined;
	RecipesService.getUnits().then(function(response) {
		$scope.units = response.data;
		$scope.currentIngredient.unit = $scope.units[0];
	});

	$scope.addIngredient = function() {
		$scope.recipe.ingredients.push(angular.copy($scope.currentIngredient));

		$scope.currentIngredient.unit = $scope.units[0];
		$scope.currentIngredient.amount = '';
		$scope.chosenIngredient = undefined;
	}

	$scope.removeIngredient = function(index) {
		$scope.recipe.ingredients.splice(index, 1);
	}
	/* ------/ STEP 02 /------ */



	/* ------ STEP 03 ------ */


	/* The steps to create the recipe */
	$scope.newStep = {};
	$scope.addStep = function() {
		$scope.recipe.steps.push($scope.newStep);
		$scope.newStep = {};
	}

	$scope.removeStep = function(index) {
		$scope.recipe.steps.splice(index, 1);
	}

	/* Set timer modal */

	$scope.setTimerModal = function (index) {

		var modalInstance = $modal.open({
			templateUrl: 'js/modules/recipes/modals/partials/modal-set-timer.html',
			controller: 'ModalSetTimerController',
			size: 'md',
			resolve: {
				data: function () {
					return {timer: $scope.recipe.steps[index].timer, index: index};						
				}
			}

		});

		modalInstance.result.then(function (data) {

			$scope.recipe.steps[data.index].timer = data.timer;
		
		}, function () {
			console.log('Modal dismissed at: ' + new Date());
		});
	};



	/* ------/ STEP 03 /------ */

	/* Upload */

	$scope.uploading = false;
	$scope.onFileSelect = function($files) {

		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			$scope.upload = $upload.upload({
				url: $scope.api + '/recipes/save-image',
				method: 'POST',
				file: file,
			}).progress(function(evt) {
				var percent = parseInt(100.0 * evt.loaded / evt.total);
				if (percent !== 100) {
					$scope.uploading = true;
				}
				else {
					$scope.uploading = false;
				}
				console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
			}).success(function(data, status, headers, config) {
				data.hovered = false;
				console.log(data);
				$scope.recipe.images.push(data);
				$scope.changeMainImage($scope.recipe.images[0]);
				$scope.uploadError = undefined;
			}).error(function(data) {
				console.log(data);
				$scope.uploadError = data;
			});
		}
	};

	$scope.changeMainImage = function(image) {
		angular.forEach($scope.recipe.images, function(loopedImage) {
			loopedImage.main = false;
		});

		image.main = true;

		$scope.mainImage = image;
	}

	$scope.removeImage = function(image) {
		console.log('removing');

		var index = $scope.recipe.images.indexOf(image);

		$scope.recipe.images.splice(index, 1);

		if ($scope.recipe.images.length > 0) {
			if ($scope.mainImage == image) {
				$scope.mainImage = $scope.recipe.images[0];
			}			
		}
		else {
			$scope.mainImage = undefined;
		}
	}

	$scope.imageToggleHover = function(image) {
		image.hovered = !image.hovered;
	}


	/* ------ SUBMIT THAT SHI ------ */

	$scope.submitRecipe = function() {

		var recipe = angular.copy($scope.recipe);
		recipe.category = recipe.category.id;
		recipe.origin = recipe.origin.id;

		var temporaryTags = [];
		angular.forEach(recipe.tags, function(tag) {
			temporaryTags.push(tag.id);
		});
		recipe.tags = temporaryTags;

		var temporaryIngredients = [];
		angular.forEach(recipe.ingredients, function(ingredient) {
			temporaryIngredients.push({name: ingredient.name, amount: ingredient.amount, unit: ingredient.unit.id})
		});
		recipe.ingredients = temporaryIngredients;

		var temporaryImages = [];
		angular.forEach(recipe.images, function(image) {
			temporaryImages.push({ path: image.path, main: image.main});
		});
		recipe.images = temporaryImages;

		var temporarySteps = [];
		angular.forEach(recipe.steps, function(step) {
			if (step.timer != undefined && step.timer > 0)
				temporarySteps.push(step);
			else
				temporarySteps.push({ description: step.description });
		});
		recipe.steps = temporarySteps;


		RecipesService.saveRecipe(recipe).then(function(response) {
			console.log(response.data);
			var recipe = response.data;
			$state.go('recipe', {recipeId: recipe.id, recipeName: $scope.getSlugified(recipe.name)});
		});



	}


})








RecipesModule.controller('RecipeController', function($scope, data, RecipesService, $ionicModal, $ionicSlideBoxDelegate) {
	console.log('In');

	$scope.recipes = data.extras;
	$scope.recipe = data.recipe;
	$scope.user = data.recipe.user;

	$scope.currentImage = $scope.recipe.images[0];

	$scope.subImages = false;
	$scope.toggleSubImages = function() {
		$scope.subImages = !$scope.subImages;
	};
	$scope.changeImage = function(image) {
		$scope.currentImage = image;
	};

	$scope.scrollLeft = function() {
		$scope.scrollingLeft = true;
	};

	$scope.persons = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	$scope.currentPersons = parseInt($scope.recipe.persons);

	$scope.changePersons = function(persons) {
		var oldPersons = $scope.recipe.persons;
		$scope.recipe.persons = persons;
		angular.forEach($scope.recipe.ingredients, function(ingredient) {
			var onePerson = ingredient.amount / oldPersons;
			ingredient.amount = Math.round((onePerson * persons) * 10) / 10;
		});

	};

	// Format the tooltip for timers
	angular.forEach($scope.recipe.steps, function(step) {
		if (step.timer && step.timer > 0) {
			var timer = $scope.convertSecondsToTime(step.timer);
			var tooltip = '';

			if (timer.hours > 0) {
				tooltip = tooltip + timer.hours + ' timer ';
			}
			if (timer.minutes > 0) {
				tooltip = tooltip + timer.minutes + ' minutter ';
			}
			if (timer.seconds > 0) {
				tooltip = tooltip + timer.seconds + ' sekunder ';
			}
			step.tooltip = tooltip.trim();
		}
	});

	$scope.startTimer = function (seconds) {

		var modalInstance = $modal.open({
			templateUrl: 'js/modules/recipes/modals/partials/modal-timer.html',
			controller: 'ModalTimerController',
			size: 'md',
			backdrop: 'static',
			resolve: {
				seconds: function () {
					return seconds;
				}
			}

		});

		modalInstance.result.then(function (data) {
			$scope.recipe.steps[data.index].timer = data.timer;
		}, function () {
			console.log('Modal dismissed at: ' + new Date());
		});
	};


	$scope.ingredientLimit = 4;

	$scope.toggleIngredients = function(){
		if ($scope.ingredientLimit == 4)
			$scope.ingredientLimit = 99;
		else
			$scope.ingredientLimit = 4;
	};

	$scope.addComment = function($event) {
		if ($scope.comment.length > 0) {
			RecipesService.addComment($scope.recipe, $scope.comment).then(function(response) {
				console.log(response);
				$scope.recipe.comments.push(response.data);
			});

			$scope.comment = "";
		}

		$event.preventDefault();
	};

	$scope.loadingComments = false;
	$scope.getMoreComments = function() {
		if ($scope.loadingComments) return;
		$scope.loadingComments = true;

		RecipesService.getMoreComments($scope.recipe['id'], $scope.recipe.comments.length).then(function(response) {
			angular.forEach(response.data, function(value, key) {
			  $scope.recipe.comments.push(value);
			});
			$scope.loadingComments = false;
		});
	};



/*
	$scope.addToShoppingList = function() {
		var modalInstance = $modal.open({
			templateUrl: 'js/modules/shopping-list/modals/partials/modal-add-to-shopping-list.html',
			controller: 'ModalAddToShoppingListController',
			size: 'md',
			resolve: {
				recipe: function () {
					return $scope.recipe;
				}
			}

		});

		modalInstance.result.then(function (data) {
		}, function () {
			console.log('Modal dismissed at: ' + new Date());
		});
	}
	*/

})
RecipesModule.service('RecipesService', ['$rootScope', '$http', 
	function($rootScope, $http) {

		this.getRecipe = function() {
			
		}

	}
]);
RecipesModule.controller('RecipesController', function($scope, recipes, $state, RecipesService) {
console.log('RecipesController has been loaded.');
	$scope.recipes = recipes;
	$scope.filters = {categories: [], origins: []};

	$scope.categories = [];
	RecipesService.getCategories().then(function(response) {
		$scope.categories = response.data;
	});

	$scope.origins = [];
	RecipesService.getOrigins().then(function(response) {
		$scope.origins = response.data;
		$scope.origin = $scope.origins[42];
	});

	$scope.ingredients = [];

	$scope.$watch('ingredient.selected', function(ingredient) {

		RecipesService.getIngredients(ingredient).then(function(response) {
			console.log(response.data);
			$scope.ingredients = response.data;
		});
	});



	$scope.toggleCategory = function(category) {
		var index = $scope.filters.categories.indexOf(category.id);

		if (index > -1)
			$scope.filters.categories.splice(index, 1);
		else
			$scope.filters.categories.push(category.id);

		$scope.updateFilters();
	}

	$scope.toggleOrigin = function(origin) {
		var index = $scope.filters.origins.indexOf(origin.id);

		if (index > -1)
			$scope.filters.origins.splice(index, 1);
		else
			$scope.filters.origins.push(origin.id);

		$scope.updateFilters();
	}

	$scope.updateFilters = function() {

		RecipesService.searchRecipes($scope.filters).then(function(response) {
			console.log(response);
			$scope.recipes = response.data;
		});
	}

});
RecipesModule.service('RecipesService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getRecipes = function() {
			return $http.get($rootScope.api + '/recipes');
		};

		this.searchRecipes = function(filters) {
			return $http.post($rootScope.api + '/recipes/search-recipes', filters);
		};

		this.getRecipe = function(recipeId) {
			return $http.get($rootScope.api + '/recipes/recipe/' + recipeId + '/0');
		};

		this.getOrigins = function() {
			return $http.get($rootScope.api + '/origins');
		};

		this.getTags = function() {
			return $http.get($rootScope.api + '/tags');
		};

		this.getCategories = function() {
			return $http.get($rootScope.api + '/categories');
		};

		this.getIngredients = function(ingredient) {
			return $http.get($rootScope.api + '/ingredients/' + ingredient);
		};

		this.getUnits = function() {
			return $http.get($rootScope.api + '/units');
		};

		this.saveRecipe = function(recipe) {
			return $http.post($rootScope.api + '/recipes/save', {
				name: recipe.name,
				persons: recipe.persons,
				introduction: recipe.introduction,
				hours: recipe.hours,
				minutes: recipe.minutes,
				freezable: recipe.freezable,
				origin: recipe.origin,
				category: recipe.category,
				tags: recipe.tags,
				ingredients: recipe.ingredients,
				steps: recipe.steps,
				images: recipe.images
			});
		};

		this.rateRecipe = function(recipe, rating) {
			return $http.post($rootScope.api + '/recipes/rate', {
				recipe_id: recipe.id,
				rating: rating
			});
		};

		this.addComment = function(recipe, comment) {
			return $http.post($rootScope.api + '/recipes/save-comment', {
				recipe_id: recipe.id,
				comment: comment
			});
		};

		this.getMoreComments = function(recipeId, skip) {
			return $http.get($rootScope.api + '/recipes/more/' + recipeId + '/' + skip);
		}

	}

]);
RecipesModule.controller('SearchController', function($scope, recipes) {

    $scope.recipes = recipes;

    $scope.toggleActive = function(recipe) {
        recipe.active = !recipe.active;
    }

})
ShoppingListModule.controller('ShoppingListController', function($scope, ShoppingListService, shoppingList) {

	$scope.shoppingList = shoppingList;
	console.log(shoppingList);
	$scope.doRefresh = function() {
		ShoppingListService.getShoppingList(shoppingList.id).then(function(response) {
			$scope.shoppingList = response.data;
			$scope.$broadcast('scroll.refreshComplete');
		});
	}

	$scope.changedIngredient = function(ingredient) {
		ShoppingListService.setPurchased(shoppingList.id, ingredient.id, ingredient.purchased);
	}

})
ShoppingListModule.service('ShoppingListService', ['$rootScope', '$http', 
	function($rootScope, $http) {

		this.getShoppingLists = function() {
			return $http.get($rootScope.api + '/shopping-lists');
		}

		this.getShoppingList = function(shoppingListId) {
			return $http.get($rootScope.api + '/shopping-lists/shopping-list/' + shoppingListId);
		}

		this.getNames = function() {
			return $http.get($rootScope.api + '/shopping-lists/names');
		}

		this.create = function(name) {
			return $http.post($rootScope.api + '/shopping-lists/save', {'name': name});
		}

		this.addRecipe = function(shoppingListId, recipeId, persons) {
			return $http.post($rootScope.api + '/shopping-lists/add-recipe', {
				shoppingList: shoppingListId,
				recipe: recipeId,
				persons: persons
			});
		}

		// IngredientEntry er ID´et på shopping_lists_ingredients.id, IKKE shopping_lists_ingredients.ingredient_id
		this.setPurchased = function(shoppingListId, ingredientEntry, purchased) {
			return $http.post($rootScope.api + '/shopping-lists/set-purchased', {
				shoppingList: shoppingListId,
				ingredient: ingredientEntry,
				purchased: purchased
			});			
		}

	}
]);
ShoppingListModule.controller('ShoppingListsController', function($scope, ShoppingListService, shoppingLists) {

	$scope.shoppingLists = shoppingLists;

	angular.forEach(shoppingLists, function(shoppingList) {
		
		var ingredients = [];
		
		angular.forEach(shoppingList.recipes, function(recipe) {
			angular.forEach(recipe.ingredients, function(ingredient) {
				ingredients.push(ingredient);
			});
		});

		shoppingList.ingredients = ingredients;
	});

})
SinglepagesModule.controller('FeatureController', function($scope) {

})
SinglepagesModule.controller('FrontpageController', function($scope) {

})
UsersModule.controller('UserController', function($scope, user, $state) {

	$scope.user = user;

	$scope.tabs = [
		{ title: 'Strøm', active: true, content: 'Åh nej der er strøm' },
		{ title: 'Opskrifter' },
		{ title: 'Kogebøger' },
		{ title: 'Bedrifter' },
	];

})
UsersModule.controller('UserFeedController', function($scope, data) {

	var recipes = data.recipes;
    var badges = data.badges;

    angular.forEach(recipes, function(recipe) {
    	recipe.feed_type = 'recipe';
    });

    angular.forEach(badges, function(badge) {
    	badge.feed_type = 'badge';
    });

    $scope.activities = recipes.concat(badges);

    console.log($scope.activities);

})
UsersModule.service('UserService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getUser = function(user_id) {
			return $http.get($rootScope.api + '/users/user/' + user_id);
		}

		this.getBadges = function(user_id) {
			return $http.get($rootScope.api + '/users/badges/' + user_id);
		}

		this.getFeed = function(user_id) {
			return $http.get($rootScope.api + '/users/feed/' + user_id);
		}

	}

]);
RecipesModule.directive('cookbook', [function(){

	return {
		
		scope: {
		 	cookbook: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/cookbooks/directives/templates/cookbook.html',

		link: function($scope) {
		    
		}
	};
}]);
RecipesModule.directive('recipeBig', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/recipe-big.html',

		link: function($scope) {
		    
		}
	};
}]);
RecipesModule.directive('recipeWide', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/recipe-wide.html',

		link: function($scope) {
		    
		}
	};
}]);
RecipesModule.directive('recipe', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/recipe.html',

		link: function($scope) {
		    
		}
	};
}]);
RecipesModule.directive('searchSidebar', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/search-sidebar.html',

		link: function($scope) {
	        $scope.ingredientsInclude = [
		        {id: 1, name: "Chokolade"},
		        {id: 2, name: "Jordbær"}
		    ];

		    $scope.ingredientsExclude = [
		        {id: 1, name: "Tomat"},
		        {id: 2, name: "Gluten"}
		    ];

		    $scope.courses = [
		    	{id: 1, name: "Morgenmad"},
		    	{id: 2, name: "Middagsmad"},
		    	{id: 3, name: "Aftensmad"},
		    	{id: 4, name: "Dessert"},
		    	{id: 5, name: "Brunch"},
		    	{id: 6, name: "Suppe"},
		    	{id: 7, name: "Appetizers"},
		    	{id: 8, name: "Salater"},
		    	{id: 9, name: "Drinks"},
		    	{id: 10, name: "Hovedretter"},
		    		
		    ];

		    $scope.coursesExpandText = "Se flere";
		    $scope.coursesLimit = 5;
		    $scope.isCourseOpen = false;

		    $scope.toggleCourses = function(){
		    	$scope.isCourseOpen = !$scope.isCourseOpen;
		    	if($scope.isCourseOpen){
		    		$scope.coursesLimit = $scope.courses.length;
		    		$scope.coursesExpandText = "Se mindre";
		    	}else
		    	{
		    		$scope.coursesLimit = 5;
		    		$scope.coursesExpandText = "Se flere";
		    	}
		    };

		    $scope.allergies = [
		    	{id: 1, name: "Fisk og skaldyr"},
		    	{id: 2, name: "Sesame"},
		    	{id: 3, name: "Soya"},
		    	{id: 4, name: "Peanuts"},
		    	{id: 5, name: "Hvalnød"},
		    	{id: 6, name: "Æg"},
		    	{id: 7, name: "Mejeri"},
		    	{id: 8, name: "Gluten"},
		    	{id: 9, name: "Hvede"},
		    ];

		    $scope.allergiesExpandText = "Se flere";
		    $scope.allergiesLimit = 5;
		    $scope.isAllergiesOpen = false;

		    $scope.toggleAllergies = function(){
		    	$scope.isAllergiesOpen = !$scope.isAllergiesOpen;
		    	if($scope.isAllergiesOpen){
		    		$scope.allergiesLimit = $scope.allergies.length;
		    		$scope.allergiesExpandText = "Se mindre";
		    	}else
		    	{
		    		$scope.allergiesLimit = 5;
		    		$scope.allergiesExpandText = "Se flere";
		    	}
		    };
		}
	};
}]);
RecipesModule.controller('ModalSetTimerController', function($scope, $modalInstance, data) {

	$scope.timer = {
		hours: 0,
		minutes: 0,
		seconds: 0
	};

	if (data.timer) {
		$scope.timer = $scope.convertSecondsToTime(data.timer);
		$scope.predefined = true;
	}

	$scope.addTimer = function() {
		var timer = $scope.timer;
		if (!angular.isNumber(timer.hours) || !angular.isNumber(timer.minutes) || !angular.isNumber(timer.seconds))
			return;

		$modalInstance.close({timer: $scope.calculateSeconds(), index: data.index});
	}

	$scope.calculateSeconds = function() {
		var timer = $scope.timer;
		var seconds = 0;
		seconds = seconds + timer.hours * 60 * 60;
		seconds = seconds + timer.minutes * 60;
		seconds = seconds + timer.seconds;

		return seconds;
	}



	$scope.removeTimer = function() {
		$modalInstance.close({timer: null, index: data.index});
	}

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	}

})
RecipesModule.controller('ModalTimerController', function($scope, $modalInstance, seconds, $interval, ngAudio) {

	$scope.timer = $scope.convertSecondsToTime(seconds);

	var sound = ngAudio.load("sounds/alarm.mp3");

	var originalSeconds = seconds;
	$scope.seconds = 120;
	
	$scope.running = false;
	var intervalTimer;

	$scope.playingSound = false;

	var updateTimer = function() {
		$scope.seconds--;
		$scope.timer = $scope.convertSecondsToTime($scope.seconds);
		console.log($scope.seconds);

		if ($scope.seconds == 0) {
			$scope.running = false;
			$interval.cancel(intervalTimer);
			sound.play();
			$scope.playingSound = true;
		}
	}

	$scope.stopSound = function() {
		$scope.playingSound = false;
		sound.stop();
	}

	$scope.startTimer = function() {
		$scope.running = true;
		intervalTimer = $interval(updateTimer, 50);		
	}

	$scope.pauseTimer = function() {
		$scope.running = false;
		$interval.cancel(intervalTimer);
	}

	$scope.resetTimer = function() {
		$scope.stopSound();
		$scope.seconds = originalSeconds;
		$scope.timer = $scope.convertSecondsToTime(seconds);
		$interval.cancel(intervalTimer);
	}


	$scope.cancel = function() {
		$scope.pauseTimer();
		$scope.stopSound();
		$modalInstance.dismiss('cancel');
	}


})
ShoppingListModule.controller('ModalAddToShoppingListController', function($scope, $modalInstance, ShoppingListService, recipe) {

	$scope.recipe = recipe;

	$scope.shoppingListName = "";

	$scope.shoppingLists = [];
	var populateShoppingLists = function() {
		console.log('Going');
		ShoppingListService.getNames().then(function(response) {
			$scope.shoppingLists = response.data;
			console.log($scope.shoppingLists);
		});
	}
	populateShoppingLists();

	$scope.creating = false;
	$scope.createShoppingList = function() {
		$scope.creating = true;
		ShoppingListService.create($scope.shoppingListName).then(function(response) {

			ShoppingListService.addRecipe(response.data, recipe.id, recipe.persons).then(function(response) {
				$scope.creating = false;
				$modalInstance.close();
			});

		});
	}

	$scope.addToShoppingList = function() {
		$scope.creating = true;
		ShoppingListService.addRecipe($scope.chosenShoppingList.id, recipe.id, recipe.persons).then(function(response) {
			$scope.creating = false;
			$modalInstance.close();
		});		
	}

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	}

})
RecipesModule.directive('userSidebar', [function(){

	return {
		
		scope: {
		 	user: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/users/directives/templates/user-sidebar.html',

		link: function($scope) {
		    
		}
	};
}]);