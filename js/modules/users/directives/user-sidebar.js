RecipesModule.directive('userSidebar', [function(){

	return {
		
		scope: {
		 	user: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/users/directives/templates/user-sidebar.html',

		link: function($scope) {
		    
		}
	};
}]);