UsersModule.controller('UserController', function($scope, user, $state) {

	$scope.user = user;

	$scope.tabs = [
		{ title: 'Strøm', active: true, content: 'Åh nej der er strøm' },
		{ title: 'Opskrifter' },
		{ title: 'Kogebøger' },
		{ title: 'Bedrifter' },
	];

})