UsersModule.controller('UserFeedController', function($scope, data) {

	var recipes = data.recipes;
    var badges = data.badges;

    angular.forEach(recipes, function(recipe) {
    	recipe.feed_type = 'recipe';
    });

    angular.forEach(badges, function(badge) {
    	badge.feed_type = 'badge';
    });

    $scope.activities = recipes.concat(badges);

    console.log($scope.activities);

})