AuthenticationModule.service('AuthenticationFacebookService', ['$rootScope', 'Facebook',
	function($rootScope, Facebook) {

		this.login = function() {

	        return Facebook.login(function(response) {
	        }, {scope: 'email'});
		};

		this.logout = function() {
			Facebook.logout(function(response) {
				console.debug('OK');
			}, function(response) {
				console.debug('Noes');
			});
		}



	}
]);