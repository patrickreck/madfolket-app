AuthenticationModule.controller('AuthenticationController', function($scope, AuthenticationService, AuthenticationFacebookService) {

    $scope.login = function() {

    	AuthenticationFacebookService.login().then(function(response) {   
            if (response.authResponse) {

	            var token = response.authResponse.accessToken;
		        return AuthenticationService.login(token);

            }
    	});

    };

    $scope.logout = function() {
        return AuthenticationService.logout();
    }

    $scope.isLoggedIn = function() {
        return AuthenticationService.isLoggedIn();
    }
});
