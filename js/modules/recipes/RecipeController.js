RecipesModule.controller('RecipeController', function($scope, data, RecipesService, $ionicModal, $ionicSlideBoxDelegate) {
	console.log('In');

	$scope.recipes = data.extras;
	$scope.recipe = data.recipe;
	$scope.user = data.recipe.user;

	$scope.currentImage = $scope.recipe.images[0];

	$scope.subImages = false;
	$scope.toggleSubImages = function() {
		$scope.subImages = !$scope.subImages;
	};
	$scope.changeImage = function(image) {
		$scope.currentImage = image;
	};

	$scope.scrollLeft = function() {
		$scope.scrollingLeft = true;
	};

	$scope.persons = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	$scope.currentPersons = parseInt($scope.recipe.persons);

	$scope.changePersons = function(persons) {
		var oldPersons = $scope.recipe.persons;
		$scope.recipe.persons = persons;
		angular.forEach($scope.recipe.ingredients, function(ingredient) {
			var onePerson = ingredient.amount / oldPersons;
			ingredient.amount = Math.round((onePerson * persons) * 10) / 10;
		});

	};

	// Format the tooltip for timers
	angular.forEach($scope.recipe.steps, function(step) {
		if (step.timer && step.timer > 0) {
			var timer = $scope.convertSecondsToTime(step.timer);
			var tooltip = '';

			if (timer.hours > 0) {
				tooltip = tooltip + timer.hours + ' timer ';
			}
			if (timer.minutes > 0) {
				tooltip = tooltip + timer.minutes + ' minutter ';
			}
			if (timer.seconds > 0) {
				tooltip = tooltip + timer.seconds + ' sekunder ';
			}
			step.tooltip = tooltip.trim();
		}
	});

	$scope.startTimer = function (seconds) {

		var modalInstance = $modal.open({
			templateUrl: 'js/modules/recipes/modals/partials/modal-timer.html',
			controller: 'ModalTimerController',
			size: 'md',
			backdrop: 'static',
			resolve: {
				seconds: function () {
					return seconds;
				}
			}

		});

		modalInstance.result.then(function (data) {
			$scope.recipe.steps[data.index].timer = data.timer;
		}, function () {
			console.log('Modal dismissed at: ' + new Date());
		});
	};


	$scope.ingredientLimit = 4;

	$scope.toggleIngredients = function(){
		if ($scope.ingredientLimit == 4)
			$scope.ingredientLimit = 99;
		else
			$scope.ingredientLimit = 4;
	};

	$scope.addComment = function($event) {
		if ($scope.comment.length > 0) {
			RecipesService.addComment($scope.recipe, $scope.comment).then(function(response) {
				console.log(response);
				$scope.recipe.comments.push(response.data);
			});

			$scope.comment = "";
		}

		$event.preventDefault();
	};

	$scope.loadingComments = false;
	$scope.getMoreComments = function() {
		if ($scope.loadingComments) return;
		$scope.loadingComments = true;

		RecipesService.getMoreComments($scope.recipe['id'], $scope.recipe.comments.length).then(function(response) {
			angular.forEach(response.data, function(value, key) {
			  $scope.recipe.comments.push(value);
			});
			$scope.loadingComments = false;
		});
	};



/*
	$scope.addToShoppingList = function() {
		var modalInstance = $modal.open({
			templateUrl: 'js/modules/shopping-list/modals/partials/modal-add-to-shopping-list.html',
			controller: 'ModalAddToShoppingListController',
			size: 'md',
			resolve: {
				recipe: function () {
					return $scope.recipe;
				}
			}

		});

		modalInstance.result.then(function (data) {
		}, function () {
			console.log('Modal dismissed at: ' + new Date());
		});
	}
	*/

})