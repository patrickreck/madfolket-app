RecipesModule.directive('searchSidebar', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/search-sidebar.html',

		link: function($scope) {
	        $scope.ingredientsInclude = [
		        {id: 1, name: "Chokolade"},
		        {id: 2, name: "Jordbær"}
		    ];

		    $scope.ingredientsExclude = [
		        {id: 1, name: "Tomat"},
		        {id: 2, name: "Gluten"}
		    ];

		    $scope.courses = [
		    	{id: 1, name: "Morgenmad"},
		    	{id: 2, name: "Middagsmad"},
		    	{id: 3, name: "Aftensmad"},
		    	{id: 4, name: "Dessert"},
		    	{id: 5, name: "Brunch"},
		    	{id: 6, name: "Suppe"},
		    	{id: 7, name: "Appetizers"},
		    	{id: 8, name: "Salater"},
		    	{id: 9, name: "Drinks"},
		    	{id: 10, name: "Hovedretter"},
		    		
		    ];

		    $scope.coursesExpandText = "Se flere";
		    $scope.coursesLimit = 5;
		    $scope.isCourseOpen = false;

		    $scope.toggleCourses = function(){
		    	$scope.isCourseOpen = !$scope.isCourseOpen;
		    	if($scope.isCourseOpen){
		    		$scope.coursesLimit = $scope.courses.length;
		    		$scope.coursesExpandText = "Se mindre";
		    	}else
		    	{
		    		$scope.coursesLimit = 5;
		    		$scope.coursesExpandText = "Se flere";
		    	}
		    };

		    $scope.allergies = [
		    	{id: 1, name: "Fisk og skaldyr"},
		    	{id: 2, name: "Sesame"},
		    	{id: 3, name: "Soya"},
		    	{id: 4, name: "Peanuts"},
		    	{id: 5, name: "Hvalnød"},
		    	{id: 6, name: "Æg"},
		    	{id: 7, name: "Mejeri"},
		    	{id: 8, name: "Gluten"},
		    	{id: 9, name: "Hvede"},
		    ];

		    $scope.allergiesExpandText = "Se flere";
		    $scope.allergiesLimit = 5;
		    $scope.isAllergiesOpen = false;

		    $scope.toggleAllergies = function(){
		    	$scope.isAllergiesOpen = !$scope.isAllergiesOpen;
		    	if($scope.isAllergiesOpen){
		    		$scope.allergiesLimit = $scope.allergies.length;
		    		$scope.allergiesExpandText = "Se mindre";
		    	}else
		    	{
		    		$scope.allergiesLimit = 5;
		    		$scope.allergiesExpandText = "Se flere";
		    	}
		    };
		}
	};
}]);