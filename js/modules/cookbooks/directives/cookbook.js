RecipesModule.directive('cookbook', [function(){

	return {
		
		scope: {
		 	cookbook: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/cookbooks/directives/templates/cookbook.html',

		link: function($scope) {
		    
		}
	};
}]);