MadfolketApp.config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app', {
				url: '/app',
				abstract: true,
				templateUrl: 'templates/menu.html'
			})

			.state('app.recipes', {
				url: '/recipes',
				views: {
					'menuContent': {
						templateUrl: 'js/modules/recipes/partials/recipes.html',
						controller: 'RecipesController',
						resolve: {
							recipes : ['RecipesService', '$q', function(RecipesService, $q) {
								var defer = $q.defer();

								RecipesService.getRecipes().then(function(response) {
									console.log(response.data);
									defer.resolve(response.data);
								});

								return defer.promise;
							}]
						}
					}
				}
			})

			.state('app.recipe', {
				url: '/recipes/:recipeId',
				views: {
					'menuContent': {
						templateUrl: 'js/modules/recipes/partials/recipe.html',
						controller: 'RecipeController',
						resolve: {
							data : ['RecipesService', '$q', '$stateParams', function(RecipesService, $q, $stateParams) {
								var defer = $q.defer();

								RecipesService.getRecipe($stateParams.recipeId).then(function(response) {
									console.log(response.data);
									defer.resolve(response.data);
								});

								return defer.promise;
							}]
						}
					}
				}
			})
			.state('app.search', {
				url: '/search',
				views: {
					'menuContent': {
						templateUrl: 'templates/search.html'
					}
				}
			})

			.state('app.browse', {
				url: '/browse',
				views: {
					'menuContent': {
						templateUrl: 'templates/browse.html'
					}
				}
			})
			.state('app.playlists', {
				url: '/playlists',
				views: {
					'menuContent': {
						templateUrl: 'templates/playlists.html',
						controller: 'PlaylistsCtrl'
					}
				}
			})

			.state('app.single', {
				url: '/playlists/:playlistId',
				views: {
					'menuContent': {
						templateUrl: 'templates/playlist.html',
						controller: 'PlaylistCtrl'
					}
				}
			});
		// if none of the above states are matched, use this as the fallback
		$urlRouterProvider.otherwise('/app/recipes');

		//.state('app', {
		//	url: '/app',
		//	abstract: true,
		//	templateUrl: 'templates/menu.html',
		//	controller: 'AppCtrl'
		//})
		//
		//.state('app.recipes', {
		//	url: '/recipes',
		//	views: {
		//		'menuContent': {
		//			templateUrl: 'js/modules/recipes/partials/recipes.html',
		//			controller: 'RecipesController',
		//			resolve: {
		//				recipes : ['RecipesService', '$q', function(RecipesService, $q) {
		//					var defer = $q.defer();
		//
		//					RecipesService.getRecipes().then(function(response) {
		//						console.log(response.data);
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			}
		//		}
		//	}
		//});
		//
		//$urlRouterProvider.otherwise('/recipes');

		//.state('tab.recipe', {
		//	url: '/recipes/:recipeId',
		//	views: {
		//		'tab-recipes': {
		//			templateUrl: 'js/modules/recipes/partials/recipe.html',
		//			controller: 'RecipeController',
		//			resolve: {
		//				data : ['RecipesService', '$q', '$stateParams', function(RecipesService, $q, $stateParams) {
		//					var defer = $q.defer();
		//
		//					RecipesService.getRecipe($stateParams.recipeId).then(function(response) {
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			},
		//
		//		}
		//	}
		//})
		//
		//.state('tab.shopping-lists', {
		//	url: '/shopping-lists',
		//	views: {
		//		'tab-shoppinglists': {
		//			templateUrl: "js/modules/shopping-list/partials/shopping-lists.html",
		//			controller: 'ShoppingListsController',
		//			resolve: {
		//
		//				shoppingLists : ['ShoppingListService', '$q', function(ShoppingListService, $q) {
		//					var defer = $q.defer();
		//
		//					console.log('getting');
		//
		//					ShoppingListService.getShoppingLists().then(function(response) {
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			},
		//
		//		}
		//	}
		//})
		//
		//.state('tab.shopping-list', {
		//	url: '/shopping-lists/:shoppingListId',
		//	views: {
		//		'tab-shoppinglists': {
		//			templateUrl: "js/modules/shopping-list/partials/shopping-list.html",
		//			controller: 'ShoppingListController',
		//			resolve: {
		//				shoppingList: ['ShoppingListService', '$q', '$stateParams', function(ShoppingListService, $q, $stateParams) {
		//					var defer = $q.defer();
		//
		//					ShoppingListService.getShoppingList($stateParams.shoppingListId).then(function(response) {
		//					console.log('aa');
		//
		//						defer.resolve(response.data);
		//					});
		//
		//					return defer.promise;
		//				}]
		//			},
		//		}
		//	}
		//})
		//
		//.state('user', {
		//	url: '/user',
		//	template: 'user',
		//});

	}
]);