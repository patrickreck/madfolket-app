MadfolketApp.directive('ratings', ['RecipesService', function(RecipesService){

	return {

		scope: {
		 	stars: '=',
		 	changable: '=?',
		 	myrating: '=',
		 	recipe: '=',
		},

		restrict: 'E',
		templateUrl: 'js/common/directives/templates/ratings.html',

		link: function($scope) {

			$scope.starsArray = [];
			$scope.currentStars = $scope.stars;
			$scope.haveRated = angular.isNumber($scope.myrating);
			$scope.hovering = false;

			for (i = 0; i < 5; i++) {
				$scope.starsArray.push({filled: false});
			}

			$scope.hoverIn = function(hoveredStar) {
			    if (!$scope.$root.isLoggedIn())
			        return;

				$scope.hovering = true;
				setStars($scope.starsArray.indexOf(hoveredStar) + 1);
			};

			$scope.hoverOut = function() {
				$scope.hovering = false;

				// Set stars to the amount before hovering
				setStars($scope.currentStars);
			};

			$scope.rate = function(hoveredStar) {
			    if (!$scope.$root.isLoggedIn())
			        return;

				var star = $scope.starsArray.indexOf(hoveredStar) + 1;

				// Check if you are trying to reset your rating
				if ($scope.myrating == star) {
					$scope.setStars = $scope.stars;
					$scope.currentStars = $scope.stars;
					$scope.myrating = null;
					$scope.haveRated = false;

					RecipesService.rateRecipe($scope.recipe, $scope.myrating).then(function(response) {
						$scope.currentStars = response.data;
						setStars(response.data);
					});
				}

				// If not, set your rating
				else {
					$scope.myrating = star;
					$scope.currentStars = star;
					$scope.haveRated = true;

					RecipesService.rateRecipe($scope.recipe, $scope.myrating);
				}
			};

			// Set amount of filled stars
			var setStars = function(stars) {

				angular.forEach($scope.starsArray, function(star) {
					if ($scope.starsArray.indexOf(star) <= stars - 1) {
						star.filled = true;
					}
					else {
						star.filled = false;
					}
				});

			};

			// Initialize original amount of stars
			setStars($scope.stars);

			if ($scope.myrating > 0) {
				$scope.haveRated = true;
				$scope.currentStars = $scope.myrating;
				setStars($scope.myrating);
			}


		}
	};
}]);