var MadfolketApp = angular.module('Madfolket', [
	'ionic',
	'ngCordova',
	'facebook',

	'AuthenticationModule',
	'RecipesModule'
]);

MadfolketApp.config(function(FacebookProvider) {
	FacebookProvider.init('1488234771462602');
});

MadfolketApp.run(function ($ionicPlatform, $location, $rootScope, $cordovaStatusbar, $ionicLoading, $cordovaSplashscreen, $interval, AuthenticationService) {

	$rootScope.api = 'http://' + $location.host() + '/madfolket-server/public';
	//$rootScope.api = 'http://test.madfolket.dk/madfolket-server/public';

	//AuthenticationService.updateLoggedIn();

	$rootScope.getSlugified = function(string) {
		return $filter('slugify')(string);
	};

	$rootScope.isLoggedIn = function() {
		return AuthenticationService.isLoggedIn();
	};

	$rootScope.getUser = function() {
		return AuthenticationService.getUser();
	};

	$rootScope.$on('$stateChangeStart', function() {
		$ionicLoading.show({
			template: '<i class="icon ion-load-c"></i>',
			animation: 'fade-in',
			showBackdrop: true,
			showDelay: 300
		});
	});

	$rootScope.$on('$stateChangeSuccess', function() {
		$ionicLoading.hide()
	});

	$ionicPlatform.ready(function () {

		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}

		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
			$cordovaStatusbar.overlaysWebView(true);
			$cordovaStatusBar.style(1); //Light
			$cordovaStatusbar.styleColor('white');

		}
	});
});






MadfolketApp.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

	// With the new view caching in Ionic, Controllers are only called
	// when they are recreated or on app start, instead of every page change.
	// To listen for when this page is active (for example, to refresh data),
	// listen for the $ionicView.enter event:
	//$scope.$on('$ionicView.enter', function(e) {
	//});

	// Form data for the login modal
	$scope.loginData = {};

	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('templates/login.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	// Triggered in the login modal to close it
	$scope.closeLogin = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.login = function() {
		$scope.modal.show();
	};

	// Perform the login action when the user submits the login form
	$scope.doLogin = function() {
		console.log('Doing login', $scope.loginData);

		// Simulate a login delay. Remove this and replace with your login
		// code if using a login system
		$timeout(function() {
			$scope.closeLogin();
		}, 1000);
	};
})

MadfolketApp.controller('PlaylistsCtrl', function($scope) {
		$scope.playlists = [
			{ title: 'Reggae', id: 1 },
			{ title: 'Chill', id: 2 },
			{ title: 'Dubstep', id: 3 },
			{ title: 'Indie', id: 4 },
			{ title: 'Rap', id: 5 },
			{ title: 'Cowbell', id: 6 }
		];
	})

MadfolketApp.controller('PlaylistCtrl', function($scope, $stateParams) {
	});